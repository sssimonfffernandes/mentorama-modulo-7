class Embarcacoes {
  constructor (velas, motor, remos, numeroPassageiros){
    this.velas = velas;
    this.motor = motor;
    this.remos = remos;
    this.numeroPassageiros = numeroPassageiros;
  };
};

class Jangada extends Embarcacoes {};
class Barco extends Embarcacoes {}
class Navio extends Embarcacoes {}

const jangada = new Jangada(1, false, 2, 8);
console.log(jangada);

const navio = new Navio(0, true, 0, 200);
console.log(navio);

const barco = new Barco(2, true, 0, 20);
console.log(barco);